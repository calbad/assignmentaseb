﻿namespace ASEB_Assignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.timeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.heartRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cadence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.altitude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.power = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.powerBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblMonitor = new System.Windows.Forms.Label();
            this.lblMode = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblLength = new System.Windows.Forms.Label();
            this.lblInterval = new System.Windows.Forms.Label();
            this.lblMaxAlt = new System.Windows.Forms.Label();
            this.lblAvgAlt = new System.Windows.Forms.Label();
            this.lblMaxPower = new System.Windows.Forms.Label();
            this.lblAvgPower = new System.Windows.Forms.Label();
            this.lblMaxSpeed = new System.Windows.Forms.Label();
            this.lblAvgSpeed = new System.Windows.Forms.Label();
            this.lblDistance = new System.Windows.Forms.Label();
            this.lblMinHeart = new System.Windows.Forms.Label();
            this.lblMaxHeart = new System.Windows.Forms.Label();
            this.lblAvgHeart = new System.Windows.Forms.Label();
            this.lblRows = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPi = new System.Windows.Forms.Label();
            this.lblPbRight = new System.Windows.Forms.Label();
            this.lblPbLeft = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.lblNormPwr = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Upload";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(814, 291);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(73, 30);
            this.listBox1.TabIndex = 0;
            this.listBox1.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.timeDate,
            this.heartRate,
            this.speed,
            this.cadence,
            this.altitude,
            this.power,
            this.powerBalance});
            this.dataGridView1.Location = new System.Drawing.Point(156, 10);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(741, 301);
            this.dataGridView1.TabIndex = 4;
            // 
            // timeDate
            // 
            this.timeDate.HeaderText = "Time";
            this.timeDate.Name = "timeDate";
            this.timeDate.ReadOnly = true;
            // 
            // heartRate
            // 
            this.heartRate.HeaderText = "Heart Rate";
            this.heartRate.Name = "heartRate";
            this.heartRate.ReadOnly = true;
            // 
            // speed
            // 
            this.speed.HeaderText = "Speed";
            this.speed.Name = "speed";
            this.speed.ReadOnly = true;
            // 
            // cadence
            // 
            this.cadence.HeaderText = "Cadence";
            this.cadence.Name = "cadence";
            // 
            // altitude
            // 
            this.altitude.HeaderText = "Altitude";
            this.altitude.Name = "altitude";
            // 
            // power
            // 
            this.power.HeaderText = "Power";
            this.power.Name = "power";
            // 
            // powerBalance
            // 
            this.powerBalance.HeaderText = "Power Balance";
            this.powerBalance.Name = "powerBalance";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(33, 48);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(42, 13);
            this.lblVersion.TabIndex = 5;
            this.lblVersion.Text = "Version";
            // 
            // lblMonitor
            // 
            this.lblMonitor.AutoSize = true;
            this.lblMonitor.Location = new System.Drawing.Point(33, 66);
            this.lblMonitor.Name = "lblMonitor";
            this.lblMonitor.Size = new System.Drawing.Size(42, 13);
            this.lblMonitor.TabIndex = 6;
            this.lblMonitor.Text = "Monitor";
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Location = new System.Drawing.Point(34, 84);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(41, 13);
            this.lblMode.TabIndex = 7;
            this.lblMode.Text = "SMode";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(45, 102);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(30, 13);
            this.lblDate.TabIndex = 8;
            this.lblDate.Text = "Date";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(23, 120);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(52, 13);
            this.lblTime.TabIndex = 9;
            this.lblTime.Text = "StartTime";
            // 
            // lblLength
            // 
            this.lblLength.AutoSize = true;
            this.lblLength.Location = new System.Drawing.Point(35, 138);
            this.lblLength.Name = "lblLength";
            this.lblLength.Size = new System.Drawing.Size(40, 13);
            this.lblLength.TabIndex = 10;
            this.lblLength.Text = "Length";
            // 
            // lblInterval
            // 
            this.lblInterval.AutoSize = true;
            this.lblInterval.Location = new System.Drawing.Point(33, 156);
            this.lblInterval.Name = "lblInterval";
            this.lblInterval.Size = new System.Drawing.Size(42, 13);
            this.lblInterval.TabIndex = 11;
            this.lblInterval.Text = "Interval";
            // 
            // lblMaxAlt
            // 
            this.lblMaxAlt.AutoSize = true;
            this.lblMaxAlt.Location = new System.Drawing.Point(608, 345);
            this.lblMaxAlt.Name = "lblMaxAlt";
            this.lblMaxAlt.Size = new System.Drawing.Size(89, 13);
            this.lblMaxAlt.TabIndex = 18;
            this.lblMaxAlt.Text = "Maximum Altitude";
            // 
            // lblAvgAlt
            // 
            this.lblAvgAlt.AutoSize = true;
            this.lblAvgAlt.Location = new System.Drawing.Point(608, 327);
            this.lblAvgAlt.Name = "lblAvgAlt";
            this.lblAvgAlt.Size = new System.Drawing.Size(85, 13);
            this.lblAvgAlt.TabIndex = 17;
            this.lblAvgAlt.Text = "Average Altitude";
            // 
            // lblMaxPower
            // 
            this.lblMaxPower.AutoSize = true;
            this.lblMaxPower.Location = new System.Drawing.Point(757, 345);
            this.lblMaxPower.Name = "lblMaxPower";
            this.lblMaxPower.Size = new System.Drawing.Size(84, 13);
            this.lblMaxPower.TabIndex = 16;
            this.lblMaxPower.Text = "Maximum Power";
            // 
            // lblAvgPower
            // 
            this.lblAvgPower.AutoSize = true;
            this.lblAvgPower.Location = new System.Drawing.Point(757, 327);
            this.lblAvgPower.Name = "lblAvgPower";
            this.lblAvgPower.Size = new System.Drawing.Size(80, 13);
            this.lblAvgPower.TabIndex = 15;
            this.lblAvgPower.Text = "Average Power";
            // 
            // lblMaxSpeed
            // 
            this.lblMaxSpeed.AutoSize = true;
            this.lblMaxSpeed.Location = new System.Drawing.Point(463, 345);
            this.lblMaxSpeed.Name = "lblMaxSpeed";
            this.lblMaxSpeed.Size = new System.Drawing.Size(85, 13);
            this.lblMaxSpeed.TabIndex = 14;
            this.lblMaxSpeed.Text = "Maximum Speed";
            // 
            // lblAvgSpeed
            // 
            this.lblAvgSpeed.AutoSize = true;
            this.lblAvgSpeed.Location = new System.Drawing.Point(463, 327);
            this.lblAvgSpeed.Name = "lblAvgSpeed";
            this.lblAvgSpeed.Size = new System.Drawing.Size(81, 13);
            this.lblAvgSpeed.TabIndex = 13;
            this.lblAvgSpeed.Text = "Average Speed";
            // 
            // lblDistance
            // 
            this.lblDistance.AutoSize = true;
            this.lblDistance.Location = new System.Drawing.Point(155, 327);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(76, 13);
            this.lblDistance.TabIndex = 12;
            this.lblDistance.Text = "Total Distance";
            // 
            // lblMinHeart
            // 
            this.lblMinHeart.AutoSize = true;
            this.lblMinHeart.Location = new System.Drawing.Point(294, 363);
            this.lblMinHeart.Name = "lblMinHeart";
            this.lblMinHeart.Size = new System.Drawing.Size(103, 13);
            this.lblMinHeart.TabIndex = 21;
            this.lblMinHeart.Text = "Minimum Heart Rate";
            // 
            // lblMaxHeart
            // 
            this.lblMaxHeart.AutoSize = true;
            this.lblMaxHeart.Location = new System.Drawing.Point(294, 345);
            this.lblMaxHeart.Name = "lblMaxHeart";
            this.lblMaxHeart.Size = new System.Drawing.Size(106, 13);
            this.lblMaxHeart.TabIndex = 20;
            this.lblMaxHeart.Text = "Maximum Heart Rate";
            // 
            // lblAvgHeart
            // 
            this.lblAvgHeart.AutoSize = true;
            this.lblAvgHeart.Location = new System.Drawing.Point(294, 327);
            this.lblAvgHeart.Name = "lblAvgHeart";
            this.lblAvgHeart.Size = new System.Drawing.Size(102, 13);
            this.lblAvgHeart.TabIndex = 19;
            this.lblAvgHeart.Text = "Average Heart Rate";
            // 
            // lblRows
            // 
            this.lblRows.AutoSize = true;
            this.lblRows.Location = new System.Drawing.Point(10, 174);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(65, 13);
            this.lblRows.TabIndex = 22;
            this.lblRows.Text = "Total entries";
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 1;
            this.trackBar1.Location = new System.Drawing.Point(23, 272);
            this.trackBar1.Maximum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(104, 45);
            this.trackBar1.TabIndex = 25;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "km";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Current Unit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "km";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "mi";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 345);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 23);
            this.button2.TabIndex = 30;
            this.button2.Text = "View Graph";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblPi);
            this.panel1.Controls.Add(this.lblPbRight);
            this.panel1.Controls.Add(this.lblPbLeft);
            this.panel1.Location = new System.Drawing.Point(158, 391);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 38);
            this.panel1.TabIndex = 31;
            // 
            // lblPi
            // 
            this.lblPi.AutoSize = true;
            this.lblPi.Location = new System.Drawing.Point(212, 2);
            this.lblPi.Name = "lblPi";
            this.lblPi.Size = new System.Drawing.Size(109, 13);
            this.lblPi.TabIndex = 34;
            this.lblPi.Text = "Average Power Index";
            // 
            // lblPbRight
            // 
            this.lblPbRight.AutoSize = true;
            this.lblPbRight.Location = new System.Drawing.Point(3, 20);
            this.lblPbRight.Name = "lblPbRight";
            this.lblPbRight.Size = new System.Drawing.Size(150, 13);
            this.lblPbRight.TabIndex = 33;
            this.lblPbRight.Text = "Average Power Balance Right";
            // 
            // lblPbLeft
            // 
            this.lblPbLeft.AutoSize = true;
            this.lblPbLeft.Location = new System.Drawing.Point(3, 2);
            this.lblPbLeft.Name = "lblPbLeft";
            this.lblPbLeft.Size = new System.Drawing.Size(143, 13);
            this.lblPbLeft.TabIndex = 32;
            this.lblPbLeft.Text = "Average Power Balance Left";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 407);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(109, 23);
            this.button3.TabIndex = 32;
            this.button3.Text = "View Calendar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 376);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 23);
            this.button4.TabIndex = 33;
            this.button4.Text = "Save to Calendar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // lblNormPwr
            // 
            this.lblNormPwr.AutoSize = true;
            this.lblNormPwr.Location = new System.Drawing.Point(757, 363);
            this.lblNormPwr.Name = "lblNormPwr";
            this.lblNormPwr.Size = new System.Drawing.Size(92, 13);
            this.lblNormPwr.TabIndex = 34;
            this.lblNormPwr.Text = "Normalised Power";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(904, 438);
            this.Controls.Add(this.lblNormPwr);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.lblRows);
            this.Controls.Add(this.lblMinHeart);
            this.Controls.Add(this.lblMaxHeart);
            this.Controls.Add(this.lblAvgHeart);
            this.Controls.Add(this.lblMaxAlt);
            this.Controls.Add(this.lblAvgAlt);
            this.Controls.Add(this.lblMaxPower);
            this.Controls.Add(this.lblAvgPower);
            this.Controls.Add(this.lblMaxSpeed);
            this.Controls.Add(this.lblAvgSpeed);
            this.Controls.Add(this.lblDistance);
            this.Controls.Add(this.lblInterval);
            this.Controls.Add(this.lblLength);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblMode);
            this.Controls.Add(this.lblMonitor);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblMaxAlt;
        private System.Windows.Forms.Label lblAvgAlt;
        private System.Windows.Forms.Label lblMaxPower;
        private System.Windows.Forms.Label lblAvgPower;
        private System.Windows.Forms.Label lblMaxSpeed;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.Label lblMinHeart;
        private System.Windows.Forms.Label lblMaxHeart;
        private System.Windows.Forms.Label lblAvgHeart;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn heartRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn speed;
        private System.Windows.Forms.DataGridViewTextBoxColumn cadence;
        private System.Windows.Forms.DataGridViewTextBoxColumn altitude;
        private System.Windows.Forms.DataGridViewTextBoxColumn power;
        private System.Windows.Forms.DataGridViewTextBoxColumn powerBalance;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.Label lblAvgSpeed;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPbRight;
        private System.Windows.Forms.Label lblPbLeft;
        private System.Windows.Forms.Label lblPi;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        public System.Windows.Forms.ListBox listBox1;
        public System.Windows.Forms.Label lblVersion;
        public System.Windows.Forms.Label lblMonitor;
        public System.Windows.Forms.Label lblMode;
        public System.Windows.Forms.Label lblDate;
        public System.Windows.Forms.Label lblTime;
        public System.Windows.Forms.Label lblLength;
        public System.Windows.Forms.Label lblInterval;
        public System.Windows.Forms.Label lblRows;
        private System.Windows.Forms.Label lblNormPwr;
    }
}

