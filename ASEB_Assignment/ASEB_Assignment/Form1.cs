﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using System.Data.SqlServerCe;

namespace ASEB_Assignment
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Cycle Data Reader/Graph/Calendar Built by Callum Baddeley
        /// This Application reads a simple text file and converts this into a graphical interface.
        /// Allows user to change speed unit and compare data at an specified interval
        /// The data that has been read in can then be saved to a database for later use, all saved races are displayed on a calendar
        /// The data can also be displayed graphically. This graph can be interacted with to adjust averages and show heart rate zones, amongst other things.
        /// </summary>
        /// 
        //CB Connection to SQL Server Created
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEB\assignmentaseb\ASEB_Assignment\ASEBDatabase.sdf");
        public Form1()
        {
            InitializeComponent();
            mySqlConn.Open();
        }
        //CB Variables created outside methods so can be accessed throughout the program
        String data;
        public String date;
        public String time;
        String length;
        String speedUnit = "km/h";
        String distUnit = "km";
        public double avgSpeed; //CB Some variables listed as public to allow access when unit testing
        double MaxSpeed;
        public double rows;
        public double lenHours;
        double distance;
        double convert = 1.609344;
        public double mphSpeed;
        public double kmhSpeed;
        public double speedSum = 0;
        public double interval;
        public DateTime graphTime, dbTime, formTime;
        public double[] leftArray, rightArray, piArray; //CB Arrays created to store values calculated from power balance column
        public double pwrSumA, pwrSumB, pwrSumC, pwrAvgA, pwrAvgB, pwrAvgC;
        public double npRoot, normalP;

        public void averageSpeed(double aRows, double aSpeedSum)
        {                        
            aRows = Math.Round(aRows, 2); //CB Math.Round used to round variable to 2 decimal places 
            aSpeedSum = Math.Round(aSpeedSum, 2);
            avgSpeed = aSpeedSum / aRows;
            avgSpeed = Math.Round(avgSpeed, 2);
            lblAvgSpeed.Text = "Average Speed: " + avgSpeed.ToString() + " " + speedUnit;
        }

        public void normalisedPower(double a, double b, double c)
        {
            //CB Math.Pow used to get the 4th power of the variable
            a = Math.Pow(a, 4.0); 
            b = Math.Pow(b, 4.0);
            c = Math.Pow(c, 4.0);
            
            npRoot = (a + b + c) / 3;
            normalP = Math.Pow(npRoot, 1.0 / 4.0); //CB To find 4th root flip the 4th power to create a fraction
            normalP = Math.Round(normalP, 2); //CB Math.Round used to create a more user friendly figure.
        }

        public void listToGrid()
        {
            //CB any data stored in the gridView is cleared to ensure only one race is shown at a time
            dataGridView1.Rows.Clear();
            //CB Using the data extracted from listBox datetime created for use in DataGridView
            //CB Created three separate DateTime's so each can be manipulated in various ways without affecting other functions
            var dateTime = DateTime.ParseExact(date + time, "yyyyMMddHH:mm:ss.FFF", CultureInfo.InvariantCulture);
            graphTime = DateTime.ParseExact(date + time, "yyyyMMddHH:mm:ss.FFF", CultureInfo.InvariantCulture);
            formTime = DateTime.ParseExact(date + time, "yyyyMMddHH:mm:ss.FFF", CultureInfo.InvariantCulture);
            foreach (string item in listBox1.Items)
            {
                if (item != "[HRData]")
                {
                    //CB String created where datetime is joined to the row data extracted from listbox for use in dataGridView
                    String row = dateTime.ToString(" HH:mm:ss dd-MM-yyyy") + "\t" + item;
                    var text = row.Split('\t', '\n');
                    double gridSpeed;
                    gridSpeed = Convert.ToDouble(text[2]); //CB Array data that contains speed is converted to double to be able to manipulate
                    gridSpeed = gridSpeed / 10; //CB Speed data divided by 20 to be shown in correct format
                    text[2] = gridSpeed.ToString(); //CB Converted back to string so can be displayed in grid view
                    dataGridView1.Rows.Add(text);
                    dateTime = dateTime.AddSeconds(interval); //CB datetime incremented by 1 second, as this is the given interval
                    //CB Will add variable into this at later stage so can handle data with different intervals
                }

            }
            rows = dataGridView1.RowCount;
            lblRows.Text = "Total entries= " + dataGridView1.RowCount.ToString();
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                speedSum += Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value); //CB For-loop used to retrieve speed value and add this on to the previous number to calculate total speed
            }
            averageSpeed(rows, speedSum); //CB Method called to calculate average speed, passing required variables

            distance = avgSpeed * lenHours; //CB Distance calculated using the average speed passed back from method and accumulative hours calculated above
            distance = Math.Round(distance, 2);
            lblDistance.Text = "Total Distance:" + distance.ToString() + " " + distUnit;

            MaxSpeed = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToDouble(r.Cells["speed"].Value)); //CB .Max used to cycle through datagridview to find highest value in speed row
            lblMaxSpeed.Text = "Max Speed: " + MaxSpeed.ToString() + " " + speedUnit;

            double powerSum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                powerSum += Convert.ToInt32(dataGridView1.Rows[i].Cells["power"].Value);
            }
            powerSum = Math.Round(powerSum, 2);
            double avgPower = powerSum / rows;
            avgPower = Math.Round(avgPower, 2);
            lblAvgPower.Text = "Average Power: " + avgPower.ToString();

            //CB Used for normalised power calculation
            //CB Divided the amount of rows into 3 to be used in the calculation
            double pwrRowA = dataGridView1.Rows.Count / 3;
            //CB Rounded to the nearest whole number so it can be used to identify the row in the GridView
            pwrRowA = Math.Round(pwrRowA, 0);
            double pwrRowB = pwrRowA * 2;                        

            pwrSumA = pwrSumB = pwrSumC = 0;
            //CB adds together all the power values whilst i is less than the amount of the first third of rows 
            for (int i = 0; i < pwrRowA; ++i)
            {
                pwrSumA += Convert.ToInt32(dataGridView1.Rows[i].Cells["power"].Value);
            }
            //CB adds together all the power values whilst i is greater than the first thrid of rows and
            //less than the amount of the second third of rows
            for (int i = Convert.ToInt32(pwrRowA + 1); i < pwrRowB; ++i)
            {
                pwrSumB += Convert.ToInt32(dataGridView1.Rows[i].Cells["power"].Value);
            }
            //CB adds together all the power values whilst i is greater than the second thrid of rows and
            //until the last row is reached
            for (int i = Convert.ToInt32(pwrRowB + 1); i < dataGridView1.Rows.Count; ++i)
            {
                pwrSumC += Convert.ToInt32(dataGridView1.Rows[i].Cells["power"].Value);
            }

            //CB Average of each sum calculated
            pwrAvgA = pwrSumA / pwrRowA;
            pwrAvgB = pwrSumB / pwrRowA;
            pwrAvgC = pwrSumB / pwrRowA;
            
            //CB Averages passed to the normalisedPower method
            normalisedPower(pwrAvgA, pwrAvgB, pwrAvgC);
            lblNormPwr.Text = "Normalised Power: " + normalP.ToString();

            double MaxPower = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToInt32(r.Cells["power"].Value));
            lblMaxPower.Text = "Max Power: " + MaxPower.ToString();

            double heartSum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                heartSum += Convert.ToInt32(dataGridView1.Rows[i].Cells["heartRate"].Value);
            }
            heartSum = Math.Round(heartSum, 2);
            double avgHeart = heartSum / rows;
            avgHeart = Math.Round(avgHeart, 2);
            lblAvgHeart.Text = "Average Heart Rate: " + avgHeart.ToString();

            double MaxHeart = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToInt32(r.Cells["heartRate"].Value));
            lblMaxHeart.Text = "Max Heart Rate: " + MaxHeart.ToString();
            double MinHeart = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Min(r => Convert.ToInt32(r.Cells["heartRate"].Value));
            lblMinHeart.Text = "Min Heart Rate: " + MinHeart.ToString();

            double altSum = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                altSum += Convert.ToInt32(dataGridView1.Rows[i].Cells["altitude"].Value);
            }
            altSum = Math.Round(altSum, 2);
            double avgAlt = altSum / rows;
            avgAlt = Math.Round(avgAlt, 2);
            lblAvgAlt.Text = "Average Altitude: " + avgAlt.ToString();

            double MaxAlt = dataGridView1.Rows.Cast<DataGridViewRow>()
                    .Max(r => Convert.ToInt32(r.Cells["altitude"].Value));
            lblMaxAlt.Text = "Max Altitude: " + MaxAlt.ToString();

            //CB This is were power balance/pedaling index is calculated
            double leftSum, piSum, rightSum;
            leftSum = piSum = rightSum = 0;
            leftArray = new double[dataGridView1.Rows.Count]; //CB Set array length to the length of gridView
            rightArray = new double[dataGridView1.Rows.Count];
            piArray = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                if (dataGridView1.Rows[i].Cells["powerBalance"].Value != null)
                {
                    long pBalance, leftMask, piMask;
                    string binary, leftBinary, piBinary;
                    double leftValue, piValue, rightValue;
                    pBalance = Convert.ToInt64(dataGridView1.Rows[i].Cells["powerBalance"].Value); //CB converts row data into long int
                    binary = Convert.ToString(pBalance, 2).PadLeft(16, '0');//CB row data converted into a binary number 
                    leftMask = pBalance & 255; //CB left hand side masked to give only the left balance using bitwise &, 0000000011111111
                    leftBinary = Convert.ToString(leftMask, 2).PadLeft(16, '0'); //CB masked leftside now convert into its own binary number
                    leftValue = Convert.ToInt32(leftBinary, 2); //CB Left side convert into a double
                    
                    leftArray[i] = leftValue;
                    
                    leftSum += leftValue; //CB sum of all left values created
                    rightValue = 100 - leftValue; //CB Right value calculated by taking left away from 100
                    rightArray[i] = rightValue;
                    rightSum += rightValue; //CB sum of all left values created
                    piMask = pBalance & 65280; //CB another mask used to get pedaling index, 1111111100000000
                    piMask = piMask >> 8; //CB bitwise >> used to shift the value down 8 places as first bit represent 256 not 1.
                    piBinary = Convert.ToString(piMask, 2).PadLeft(16, '0');
                    piValue = Convert.ToInt32(piBinary, 2);
                    piArray[i] = piValue;
                    piSum += piValue;                    
                }
            }

            //CB Averages calculated for left, right balance and pedaling index
            leftSum = Math.Round(leftSum, 2);
            double avgLeft = leftSum / rows;
            avgLeft = Math.Round(avgLeft, 2);
            lblPbLeft.Text = "Average Power Balance Left: " + avgLeft.ToString();

            rightSum = Math.Round(rightSum, 2);
            double avgRight = rightSum / rows;
            avgRight = Math.Round(avgRight, 2);
            lblPbRight.Text = "Average Power Balance Right: " + avgRight.ToString();

            piSum = Math.Round(piSum, 2);
            double avgPi = piSum / rows;
            avgPi = Math.Round(avgPi, 2);
            lblPi.Text = "Average Power Index: " + avgPi.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear(); //CB Used to ensure only one set of data is added at a time else would cause error in program
            OpenFileDialog importDialog = new OpenFileDialog(); //CB Opens File Dialog Box

            importDialog.Filter = "All Files (*.*)|*.*"; //CB Sets filter options

            if (importDialog.ShowDialog() == DialogResult.OK)//CB added to check if open button pressed
            {                
                StreamReader reader = new StreamReader(importDialog.FileName);

                using (reader)
                {
                    bool read = false;
                    while ((data = reader.ReadLine()) != null)
                    {

                        if (data.Contains("Version")) //CB .Contains used to find specific lines of code that can be used later in the program
                        {
                            lblVersion.Text = data;
                        }
                        if (data.Contains("Monitor"))
                        {
                            lblMonitor.Text = data;
                        }
                        if (data.Contains("SMode"))
                        {
                            lblMode.Text = data;
                        }
                        if (data.Contains("Date"))
                        {
                            var text = data.Split('='); //CB Split used to create area of data so usable data can be extracted and used later
                            date = text[1];
                            lblDate.Text = text[0] + "=" + text[1];
                        }
                        if (data.Contains("StartTime"))
                        {
                            var text = data.Split('=');
                            time = text[1];
                            lblTime.Text = text[0] + "=" + text[1];

                        }
                        if (data.Contains("Length"))
                        {
                            var text = data.Split('=');                            
                            lblLength.Text = text[0] + "=" + text[1];
                            length = text[1];
                            var lenCon = text[1].Split(':', '.'); //CB Anohter split used to allow conversion of time elements to hours
                            /*
                             * CB Manual conversion of minutes, seconds, milliseconds to hours
                             * CB Then added together to be used later to calculate total distance
                             */
                            double hours = Convert.ToDouble(lenCon[0]);
                            double mHours = Convert.ToDouble(lenCon[1]);
                            mHours = mHours / 60;
                            double sHours = Convert.ToDouble(lenCon[2]);
                            sHours = sHours / 3600;
                            double msHours = Convert.ToDouble(lenCon[3]);
                            msHours = msHours / 36000;
                            lenHours = hours + mHours + sHours + msHours;
                        }
                        if (data.Contains("Interval"))
                        {
                            var text = data.Split('='); //CB Split used to create area of data so usable data can be extracted and used later
                            interval = Convert.ToDouble(text[1]);
                            lblInterval.Text = text[0] + "=" + text[1];
                        }

                        if (data.Contains("[HRData]"))
                        {
                            read = true;
                        }

                        if (read)
                        {
                            listBox1.Items.Add(data);
                        }
                    }
                }
                listToGrid();
            }
        }

        /*
         * CB Methods created to convert mph to kmh and vice versa. Convert value stored at top of program. 
         * Current unit value is passed to method.          
         */
        public void kmhToMph(double kmh)
        {
            kmh = kmh / convert;
            mphSpeed = Math.Round(kmh, 2);
        }
        public void mphToKmh(double mph)
        {
            mph = mph * convert;
            kmhSpeed = Math.Round(mph, 2);

        }
        /*
         * trackBar used to allow user to easily switch between miles and kilometres
         */
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label1.Text = trackBar1.Value.ToString();
            if (trackBar1.Value == 0) 
            {
                label1.Text = "km";
                avgSpeed = avgSpeed * convert;
                MaxSpeed = MaxSpeed * convert;
                distance = avgSpeed * lenHours;
                speedUnit = "km/h";
                distUnit = "km";
                avgSpeed = Math.Round(avgSpeed, 2);
                MaxSpeed = Math.Round(MaxSpeed, 2);
                distance = Math.Round(distance, 2); 
                lblAvgSpeed.Text = "Average Speed: " + avgSpeed.ToString() + " " + speedUnit;
                lblMaxSpeed.Text = "Max Speed: " + MaxSpeed.ToString() + " " + speedUnit;
                lblDistance.Text = "Total Distance:" + distance.ToString() + " " + distUnit;

                for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                {
                    kmhSpeed = Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value);
                    mphToKmh(kmhSpeed);
                    dataGridView1.Rows[i].Cells["speed"].Value = kmhSpeed;
                }
            }
            if (trackBar1.Value == 1)
            {
                label1.Text = "mi";
                avgSpeed = avgSpeed / convert;
                MaxSpeed = MaxSpeed / convert;
                distance = avgSpeed * lenHours;
                speedUnit = "mph";
                distUnit = "mi";
                avgSpeed = Math.Round(avgSpeed, 2);
                MaxSpeed = Math.Round(MaxSpeed, 2);
                distance = Math.Round(distance, 2); 
                lblAvgSpeed.Text = "Average Speed: " + avgSpeed.ToString() + " " + speedUnit;
                lblMaxSpeed.Text = "Max Speed: " + MaxSpeed.ToString() + " " + speedUnit;
                lblDistance.Text = "Total Distance:" + distance.ToString() + " " + distUnit;

                for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                {
                    mphSpeed = Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value);
                    kmhToMph(mphSpeed);
                    dataGridView1.Rows[i].Cells["speed"].Value = mphSpeed;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                //CB required data passed to Graph form to create graphs and work out averages
                Graph newGraph = new Graph();
                newGraph.hRate = new double[dataGridView1.Rows.Count];
                newGraph.speed = new double[dataGridView1.Rows.Count];
                newGraph.cadence = new double[dataGridView1.Rows.Count];
                newGraph.power = new double[dataGridView1.Rows.Count];
                newGraph.pbLeft = new double[dataGridView1.Rows.Count];
                newGraph.pbRight = new double[dataGridView1.Rows.Count];
                newGraph.pIndex = new double[dataGridView1.Rows.Count];
                newGraph.time = new DateTime[dataGridView1.Rows.Count];
                newGraph.rowCount = dataGridView1.Rows.Count;
                newGraph.nPower = normalP;
                newGraph.lblPower.Text = "Normalised Power: " + normalP.ToString();
                newGraph.raceSeconds = TimeSpan.FromHours(lenHours).TotalSeconds;

                for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                {
                    //newGraph.time[i] = Convert.ToDouble(graphTime.ToString("HH.mm.ss"));
                    newGraph.time[i] = this.graphTime;
                    newGraph.hRate[i] = Convert.ToDouble(this.dataGridView1.Rows[i].Cells["heartRate"].Value);
                    newGraph.speed[i] = Convert.ToDouble(this.dataGridView1.Rows[i].Cells["speed"].Value);
                    newGraph.cadence[i] = Convert.ToDouble(this.dataGridView1.Rows[i].Cells["cadence"].Value);
                    newGraph.power[i] = Convert.ToDouble(this.dataGridView1.Rows[i].Cells["power"].Value);
                    newGraph.pbLeft[i] = this.leftArray[i];
                    newGraph.pbRight[i] = this.rightArray[i];
                    newGraph.pIndex[i] = this.piArray[i];
                    graphTime = graphTime.AddSeconds(interval);
                }
                newGraph.Show();
            }
            else
            {
                //CB stops graph being accessed when no data uploaded to gridView
                MessageBox.Show("Please Upload a Race or Retrieve From Calendar");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //CB opens calendar form to upload a stored race
            CycleCal newCalendar = new CycleCal();
            newCalendar.Show();
            this.Hide();
        }

        public bool checkRace() //CB Used to check whether Account number is already in use
        {
            //CB checks to see if race is already saved
            SqlCeCommand scc = new SqlCeCommand("Select Count(*) From RaceDate where  date = '" + formTime + "'", mySqlConn);

            bool rtnvalue = true;
            using (SqlCeDataReader mySqlDR = scc.ExecuteReader())
            {
                DataTable dt = new DataTable();
                dt.Load(mySqlDR);
                if (dt.Rows[0][0].ToString() == "1")
                {                    
                    rtnvalue = false;
                }

                return (rtnvalue);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 1)
            {
                if (checkRace())
                {
                    try
                    {
                        //CB if checkRace() is passed race date is added to database...
                        SqlCeCommand cmd = new SqlCeCommand("INSERT INTO RaceDate(date, version, monitor, smode, startDate, startTime, length, interval, entries, lenHours) VALUES (@date, @version, @monitor, @smode, @startDate, @startTime, @length, @interval, @entries, @lenHours)", mySqlConn);
                        cmd.Parameters.AddWithValue("@date", formTime);
                        cmd.Parameters.AddWithValue("@version", lblVersion.Text);
                        cmd.Parameters.AddWithValue("@monitor", lblMonitor.Text);
                        cmd.Parameters.AddWithValue("@smode", lblMode.Text);
                        cmd.Parameters.AddWithValue("@startDate", lblDate.Text);
                        cmd.Parameters.AddWithValue("@startTime", lblTime.Text);
                        cmd.Parameters.AddWithValue("@length", lblLength.Text);
                        cmd.Parameters.AddWithValue("@interval", lblInterval.Text);
                        cmd.Parameters.AddWithValue("@entries", lblRows.Text);
                        cmd.Parameters.AddWithValue("@lenHours", lenHours);
                        cmd.ExecuteNonQuery();

                        dbTime = formTime;
                        for (int i = 0; i < dataGridView1.Rows.Count; ++i)
                        {                            
                            //CB Race data added with corresponding date 
                            double originalSpeed = Convert.ToDouble(dataGridView1.Rows[i].Cells["speed"].Value) * 10;
                            SqlCeCommand cmd2 = new SqlCeCommand("INSERT INTO RaceData(date, hr, speed, cadence, altitude, power, balance, start) VALUES (@dateB, @hr, @speed, @cadence, @altitude, @power, @balance, @start)", mySqlConn);
                            cmd2.Parameters.AddWithValue("@dateB", dbTime);
                            cmd2.Parameters.AddWithValue("@hr", dataGridView1.Rows[i].Cells["heartRate"].Value ?? DBNull.Value);
                            cmd2.Parameters.AddWithValue("@speed", originalSpeed);
                            cmd2.Parameters.AddWithValue("@cadence", dataGridView1.Rows[i].Cells["cadence"].Value ?? DBNull.Value);
                            cmd2.Parameters.AddWithValue("@altitude", dataGridView1.Rows[i].Cells["altitude"].Value ?? DBNull.Value);
                            cmd2.Parameters.AddWithValue("@power", dataGridView1.Rows[i].Cells["power"].Value ?? DBNull.Value);
                            cmd2.Parameters.AddWithValue("@balance", dataGridView1.Rows[i].Cells["powerBalance"].Value ?? DBNull.Value);
                            cmd2.Parameters.AddWithValue("@start", formTime);
                            cmd2.ExecuteNonQuery();
                            dbTime = dbTime.AddSeconds(interval);
                        }
                        MessageBox.Show("Race Saved");
                    }
                    catch (SqlCeException ex)
                    {
                        MessageBox.Show(graphTime + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    //CB if race date already exist, not saved and message box shown
                    MessageBox.Show("This Race Has Already Been Saved");
                }
            }
            else
            {
                //CB checks there is data to be saved before saving
                MessageBox.Show("You Must Upload a Race Before it Can Be Saved");
            }
            
        }
    }
}
