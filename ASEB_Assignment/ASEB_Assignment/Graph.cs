﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ASEB_Assignment
{
    public partial class Graph : Form
    {
        public Graph()
        {
            InitializeComponent();
        }
        public DateTime[] time;
        public double[] hRate;
        public double[] speed;
        public double[] cadence;
        public double[] power;
        public double[] pBalance;
        public double[] pbLeft, pbRight, pIndex;
        double avgHR, avgSpeed, avgCadence, avgPower, avgLeft, avgRight, avgPedal;
        public double rowCount, result, yMin, yMax, nPower, iFactor, raceSeconds, trainingSS;

        PointPairList listHR;
        PointPairList listSpeed;
        PointPairList listCadence;
        PointPairList listPower;
        PointPairList listLeft;
        PointPairList listRight;
        PointPairList listPedal;
        FilteredPointList newHR, newSpeed, newCadence, newPower, newLeft, newRight, newPedal;
        public int y1, y2, y3, y4;
        public GraphPane myPane, myPane2;
        LineItem hrCurve, speedCurve, cadenceCurve, powerCurve;
        XDate dateFirst, dateLast;
        bool axisSwitch = false;

        private void Graph_Load(object sender, EventArgs e)
        {
            // Setup the graph
            CreateGraph(zedGraphControl1);
            CreatePie(zedGraphControl2);
            // Size the control to fill the form with a margin
            SetSize();
        }

        private void SetSize()
        {
            zedGraphControl1.Location = new Point(10, 10);
            // Leave a small margin around the outside of the control
            zedGraphControl1.Size = new Size(ClientRectangle.Width - 20, 435);
        }

        private void Graph_Resize(object sender, EventArgs e)
        {
            SetSize();
        }

        // Build the Chart
        private void CreateGraph(ZedGraphControl zgc)
        {
            //CB two XDates created to store the first and last dates of related to the data passed across from Form1
            dateFirst = new XDate(time.First().Year, time.First().Month, time.First().Day, time.First().Hour, time.First().Minute, time.First().Second);            
            dateLast = new XDate(time.Last().Year, time.Last().Month, time.Last().Day, time.Last().Hour, time.Last().Minute, time.Last().Second);            
            zedGraphControl1.IsEnableHZoom = true;
            zedGraphControl1.IsEnableVZoom = false; //CB stop vertical zooming so all curves can be seen when a certain interval is selected
            zedGraphControl1.IsShowHScrollBar = true; //CB ensures horizontal scroll bar is shown so interval can be moved across to show different parts of the graph when zoomed
            zedGraphControl1.IsAutoScrollRange = true;
            zedGraphControl1.ScrollGrace = .03;            

            myPane = zgc.GraphPane;
            //CB to see which is the primary axis for displaying of zones
            if(axisSwitch == false)
            {
                //CB y-axis created for each curve
                myPane.YAxisList.Clear();
                y1 = myPane.AddYAxis("Heart Rate");
                y2 = myPane.AddYAxis("Speed");
                y3 = myPane.AddYAxis("Cadence");
                y4 = myPane.AddYAxis("Power");
            }
            else if (axisSwitch == true)
            {
                myPane.YAxisList.Clear();
                y4 = myPane.AddYAxis("Power");
                y1 = myPane.AddYAxis("Heart Rate");
                y2 = myPane.AddYAxis("Speed");
                y3 = myPane.AddYAxis("Cadence");                
            }
                       
            //CB Set the Titles
            myPane.Title.Text = "Cycle Data for " + time[0].ToString("dd-MM-yyyy");
            myPane.XAxis.Title.Text = "Time";
            //CB Set x-axis type
            myPane.XAxis.Type = AxisType.Date;            
            myPane.XAxis.MajorGrid.IsVisible = true;
            myPane.YAxis.MajorGrid.IsVisible = true; 
            myPane.XAxis.Scale.MajorUnit = DateUnit.Minute;
            myPane.XAxis.Scale.MinorUnit = DateUnit.Minute;
            myPane.XAxis.Scale.MajorStep = 5;
            myPane.XAxis.Scale.MinorStep = 5;
            myPane.XAxis.Scale.Format = "HH.mm.ss";            

            double yHR, ySpeed, yCadence, yPower, yLeft, yRight, yPedal;
            XDate x;
            yHR = ySpeed = yCadence = yPower = yLeft = yRight = yPedal = 0;
            listHR = new PointPairList();
            listSpeed = new PointPairList();
            listCadence = new PointPairList();
            listPower = new PointPairList();
            listLeft = new PointPairList();
            listRight = new PointPairList();
            listPedal = new PointPairList();
            for (int i = 0; i < rowCount; i++)
            {
                //CB arrays created using data from Form1, linked with corresponding DateTime and added to pointpairlist
                x = new XDate(time[i].Year, time[i].Month, time[i].Day, time[i].Hour, time[i].Minute, time[i].Second);                                
                yHR = hRate[i];
                ySpeed = speed[i];
                yCadence = cadence[i];
                yPower = power[i];
                yLeft = pbLeft[i];
                yRight = pbRight[i];
                yPedal = pIndex[i];
                listHR.Add(x, yHR);
                listSpeed.Add(x, ySpeed);
                listCadence.Add(x, yCadence);
                listPower.Add(x, yPower);
                listLeft.Add(x, yLeft);
                listRight.Add(x, yRight);
                listPedal.Add(x, yPedal);
            }
            //CB all existing curves cleared
            myPane.CurveList.Clear();
            //CB curves added to to grpahpane and legend 
            hrCurve = new LineItem("Heart Rate", listHR, Color.Red, SymbolType.None) { YAxisIndex = y1 };
            myPane.CurveList.Add(hrCurve);
            speedCurve = new LineItem("Speed", listSpeed, Color.Blue, SymbolType.None) { YAxisIndex = y2 };
            myPane.CurveList.Add(speedCurve);
            cadenceCurve = new LineItem("Cadence", listCadence, Color.Gold, SymbolType.None) { YAxisIndex = y3 };
            myPane.CurveList.Add(cadenceCurve);
            powerCurve = new LineItem("Power", listPower, Color.Green, SymbolType.None) { YAxisIndex = y4 };
            myPane.CurveList.Add(powerCurve);            

            //CB each y-axis given same colour as linked curve to help interpret the graph
            myPane.Chart.Border.IsVisible = false;
            myPane.YAxisList[y1].Title.IsVisible = false;
            myPane.YAxisList[y1].Color = Color.Red;
            myPane.YAxisList[y1].Scale.FontSpec.FontColor = Color.Red;
            myPane.YAxisList[y1].MajorTic.Color = Color.Red;
            myPane.YAxisList[y1].MinorTic.Color = Color.Red;
            myPane.YAxisList[y1].MajorGrid.Color = Color.Red;
            myPane.YAxisList[y1].MinorGrid.Color = Color.Red;
            myPane.YAxisList[y2].Title.IsVisible = false;
            myPane.YAxisList[y2].Color = Color.Blue;
            myPane.YAxisList[y2].Scale.FontSpec.FontColor = Color.Blue;
            myPane.YAxisList[y2].MajorTic.Color = Color.Blue;
            myPane.YAxisList[y2].MinorTic.Color = Color.Blue;
            myPane.YAxisList[y2].MajorGrid.Color = Color.Blue;
            myPane.YAxisList[y2].MinorGrid.Color = Color.Blue;
            myPane.YAxisList[y3].Title.IsVisible = false;
            myPane.YAxisList[y3].Color = Color.Gold;
            myPane.YAxisList[y3].Scale.FontSpec.FontColor = Color.Gold;
            myPane.YAxisList[y3].MajorTic.Color = Color.Gold;
            myPane.YAxisList[y3].MinorTic.Color = Color.Gold;
            myPane.YAxisList[y3].MajorGrid.Color = Color.Gold;
            myPane.YAxisList[y3].MinorGrid.Color = Color.Gold;
            myPane.YAxisList[y4].Title.IsVisible = false;
            myPane.YAxisList[y4].Color = Color.Green;
            myPane.YAxisList[y4].Scale.FontSpec.FontColor = Color.Green;
            myPane.YAxisList[y4].MajorTic.Color = Color.Green;
            myPane.YAxisList[y4].MinorTic.Color = Color.Green;
            myPane.YAxisList[y4].MajorGrid.Color = Color.Green;
            myPane.YAxisList[y4].MinorGrid.Color = Color.Green;

            //CB arrays created of the lists to be used to create averages
            double[] hrArray = listHR.Select(p => p.Y).ToArray();
            double[] speedArray = listSpeed.Select(p => p.Y).ToArray();
            double[] cadenceArray = listCadence.Select(p => p.Y).ToArray();
            double[] powerArray = listPower.Select(p => p.Y).ToArray();
            double[] leftArray = listLeft.Select(p => p.Y).ToArray();
            double[] rightArray = listRight.Select(p => p.Y).ToArray();
            double[] pedalArray = listPedal.Select(p => p.Y).ToArray();
            double[] xvalues = listHR.Select(p => p.X).ToArray();

            //CB arrays added to filteredpointlist for future zooming
            newHR = new FilteredPointList(xvalues, hrArray);
            newSpeed = new FilteredPointList(xvalues, speedArray);
            newCadence = new FilteredPointList(xvalues, cadenceArray);
            newPower = new FilteredPointList(xvalues, powerArray);
            newLeft = new FilteredPointList(xvalues, leftArray);
            newRight = new FilteredPointList(xvalues, rightArray);
            newPedal = new FilteredPointList(xvalues, pedalArray);

            //CB averages calculated for each array
            avgHR = hrArray.Average();
            avgHR = Math.Round(avgHR, 2);
            avgSpeed = speedArray.Average();
            avgSpeed = Math.Round(avgSpeed, 2);
            avgCadence = cadenceArray.Average();
            avgCadence = Math.Round(avgCadence, 2);
            avgPower = powerArray.Average();
            avgPower = Math.Round(avgPower, 2);
            avgLeft = leftArray.Average();
            avgLeft = Math.Round(avgLeft, 2);
            avgRight = rightArray.Average();
            avgRight = Math.Round(avgRight, 2);
            avgPedal = pedalArray.Average();
            avgPedal = Math.Round(avgPedal, 2);

            label1.Text = "Average Heart Rate: " + avgHR.ToString();
            label2.Text = "Average Speed: " + avgSpeed.ToString();
            label3.Text = "Average Cadence: " + avgCadence.ToString();
            label4.Text = "Average Power: " + avgPower.ToString();
            label11.Text = "Average Pedaling Index: " + avgPedal.ToString();

            zgc.AxisChange();
        }

        private void zedGraphControl1_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            // The maximum number of point to be displayed is based on the width of the graphpane, and the visible range of the X axis
            newHR.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newSpeed.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newCadence.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPower.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newLeft.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newRight.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPedal.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            
            if (newState.Type == ZoomState.StateType.Zoom)
            {
                int zoomCount = Convert.ToInt32(newHR.Count.ToString());
                double hrTotal, speedTotal, cadenceTotal, powerTotal, leftTotal, rightTotal, pedalTotal;
                hrTotal = speedTotal = cadenceTotal = powerTotal = leftTotal = rightTotal = pedalTotal = 0;
                for (int i = 0; i < zoomCount; i++)
                {
                    //CB retrieving the co-ordinates and isolationg the y-point
                    //CB this is then used to create a total for calculating new averages
                    var zoomHR = newHR[i].ToString();
                    var hrText = zoomHR.Split(',');
                    hrText[1] = hrText[1].Trim();
                    var hrTrim = hrText[1].Split(' ');
                    hrTotal += Convert.ToDouble(hrTrim[0]);

                    var zoomSpeed = newSpeed[i].ToString();
                    var speedText = zoomSpeed.Split(',');
                    speedText[1] = speedText[1].Trim();
                    var speedTrim = speedText[1].Split(' ');
                    speedTotal += Convert.ToDouble(speedTrim[0]);

                    var zoomCadence = newCadence[i].ToString();
                    var cadenceText = zoomCadence.Split(',');
                    cadenceText[1] = cadenceText[1].Trim();
                    var cadenceTrim = cadenceText[1].Split(' ');
                    cadenceTotal += Convert.ToDouble(cadenceTrim[0]);

                    var zoomPower = newPower[i].ToString();
                    var powerText = zoomPower.Split(',');
                    powerText[1] = powerText[1].Trim();
                    var powerTrim = powerText[1].Split(' ');
                    powerTotal += Convert.ToDouble(powerTrim[0]);

                    var zoomLeft = newLeft[i].ToString();
                    var leftText = zoomLeft.Split(',');
                    leftText[1] = leftText[1].Trim();
                    var leftTrim = leftText[1].Split(' ');
                    leftTotal += Convert.ToDouble(leftTrim[0]);

                    var zoomRight = newRight[i].ToString();
                    var rightText = zoomRight.Split(',');
                    rightText[1] = rightText[1].Trim();
                    var rightTrim = rightText[1].Split(' ');
                    rightTotal += Convert.ToDouble(rightTrim[0]);

                    var zoomPedal = newPedal[i].ToString();
                    var pedalText = zoomPedal.Split(',');
                    pedalText[1] = pedalText[1].Trim();
                    var pedalTrim = pedalText[1].Split(' ');
                    pedalTotal += Convert.ToDouble(pedalTrim[0]);
                }
                avgHR = hrTotal / zoomCount;
                avgHR = Math.Round(avgHR, 2);
                avgSpeed = speedTotal / zoomCount;
                avgSpeed = Math.Round(avgSpeed, 2);
                avgCadence = cadenceTotal / zoomCount;
                avgCadence = Math.Round(avgCadence, 2);
                avgPower = powerTotal / zoomCount;
                avgPower = Math.Round(avgPower, 2);
                avgLeft = leftTotal / zoomCount;
                avgLeft = Math.Round(avgLeft, 2);
                avgRight = rightTotal / zoomCount;
                avgRight = Math.Round(avgRight, 2);
                avgPedal = pedalTotal / zoomCount;
                avgPedal = Math.Round(avgPedal, 2);

                label1.Text = "Average Heart Rate: " + avgHR.ToString();
                label2.Text = "Average Speed: " + avgSpeed.ToString();
                label3.Text = "Average Cadence: " + avgCadence.ToString();
                label4.Text = "Average Power: " + avgPower.ToString();
                label11.Text = "Average Pedaling Index: " + avgPedal.ToString();

                // This refreshes the graph when the button is released after a panning operation
                sender.Invalidate();

                myPane2.CurveList.Clear();
                //CB refreshes Power Balance based on zoomed graph
                myPane2.Title.Text = "Power Balance";
                myPane2.Title.FontSpec.Size = 30;
                myPane2.Legend.FontSpec.Size = 25;

                PieItem item1 = myPane2.AddPieSlice(avgLeft, Color.Red, 0, "Left");
                PieItem item2 = myPane2.AddPieSlice(avgRight, Color.Blue, 0, "Right");

                item1.LabelType = PieLabelType.Name_Percent;
                item1.LabelDetail.FontSpec.Size = 23;
                item2.LabelType = PieLabelType.Name_Percent;
                item2.LabelDetail.FontSpec.Size = 23;

                // Tell ZedGraph to refigure the
                // axes since the data have changed
                zedGraphControl2.AxisChange();
                zedGraphControl2.Invalidate();
                zedGraphControl2.Refresh();
            }
        }
        
        private void zedGraphControl1_ScrollProgressEvent(ZedGraphControl sender, ScrollBar scrollBar, ZoomState oldState, ZoomState newState)
        {
            // The maximum number of point to displayed is based on the width of the graphpane, and the visible range of the X axis
            newHR.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newSpeed.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newCadence.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPower.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newLeft.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newRight.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            newPedal.SetBounds(sender.GraphPane.XAxis.Scale.Min, sender.GraphPane.XAxis.Scale.Max, (int)zedGraphControl1.GraphPane.Rect.Width);
            
            if (newState.Type == ZoomState.StateType.Scroll)
            {
                int zoomCount = Convert.ToInt32(newHR.Count.ToString());
                double hrTotal, speedTotal, cadenceTotal, powerTotal, leftTotal, rightTotal, pedalTotal;
                hrTotal = speedTotal = cadenceTotal = powerTotal = leftTotal = rightTotal = pedalTotal = 0;
                for (int i = 0; i < zoomCount; i++)
                {
                    var zoomHR = newHR[i].ToString();
                    var hrText = zoomHR.Split(',');
                    hrText[1] = hrText[1].Trim();
                    var hrTrim = hrText[1].Split(' ');
                    hrTotal += Convert.ToDouble(hrTrim[0]);

                    var zoomSpeed = newSpeed[i].ToString();
                    var speedText = zoomSpeed.Split(',');
                    speedText[1] = speedText[1].Trim();
                    var speedTrim = speedText[1].Split(' ');
                    speedTotal += Convert.ToDouble(speedTrim[0]);

                    var zoomCadence = newCadence[i].ToString();
                    var cadenceText = zoomCadence.Split(',');
                    cadenceText[1] = cadenceText[1].Trim();
                    var cadenceTrim = cadenceText[1].Split(' ');
                    cadenceTotal += Convert.ToDouble(cadenceTrim[0]);

                    var zoomPower = newPower[i].ToString();
                    var powerText = zoomPower.Split(',');
                    powerText[1] = powerText[1].Trim();
                    var powerTrim = powerText[1].Split(' ');
                    powerTotal += Convert.ToDouble(powerTrim[0]);

                    var zoomLeft = newLeft[i].ToString();
                    var leftText = zoomLeft.Split(',');
                    leftText[1] = leftText[1].Trim();
                    var leftTrim = leftText[1].Split(' ');
                    leftTotal += Convert.ToDouble(leftTrim[0]);

                    var zoomRight = newRight[i].ToString();
                    var rightText = zoomRight.Split(',');
                    rightText[1] = rightText[1].Trim();
                    var rightTrim = rightText[1].Split(' ');
                    rightTotal += Convert.ToDouble(rightTrim[0]);

                    var zoomPedal = newPedal[i].ToString();
                    var pedalText = zoomPedal.Split(',');
                    pedalText[1] = pedalText[1].Trim();
                    var pedalTrim = pedalText[1].Split(' ');
                    pedalTotal += Convert.ToDouble(pedalTrim[0]);
                }
                avgHR = hrTotal / zoomCount;
                avgHR = Math.Round(avgHR, 2);
                avgSpeed = speedTotal / zoomCount;
                avgSpeed = Math.Round(avgSpeed, 2);
                avgCadence = cadenceTotal / zoomCount;
                avgCadence = Math.Round(avgCadence, 2);
                avgPower = powerTotal / zoomCount;
                avgPower = Math.Round(avgPower, 2);
                avgLeft = leftTotal / zoomCount;
                avgLeft = Math.Round(avgLeft, 2);
                avgRight = rightTotal / zoomCount;
                avgRight = Math.Round(avgRight, 2);
                avgPedal = pedalTotal / zoomCount;
                avgPedal = Math.Round(avgPedal, 2);

                label1.Text = "Average Heart Rate: " + avgHR.ToString();
                label2.Text = "Average Speed: " + avgSpeed.ToString();
                label3.Text = "Average Cadence: " + avgCadence.ToString();
                label4.Text = "Average Power: " + avgPower.ToString();
                label11.Text = "Average Pedaling Index: " + avgPedal.ToString();
                // This refreshes the graph when the button is released after a panning operation
                sender.Invalidate();

                myPane2.CurveList.Clear();
                myPane2.Title.Text = "Power Balance";
                myPane2.Title.FontSpec.Size = 30;
                myPane2.Legend.FontSpec.Size = 25;

                PieItem item1 = myPane2.AddPieSlice(avgLeft, Color.Red, 0, "Left");
                PieItem item2 = myPane2.AddPieSlice(avgRight, Color.Blue, 0, "Right");

                item1.LabelType = PieLabelType.Name_Percent;
                item1.LabelDetail.FontSpec.Size = 23;
                item2.LabelType = PieLabelType.Name_Percent;
                item2.LabelDetail.FontSpec.Size = 23;

                // Tell ZedGraph to refigure the
                // axes since the data have changed
                zedGraphControl2.AxisChange();
                zedGraphControl2.Invalidate();
                zedGraphControl2.Refresh();
            }
        }        

        //CB checkboxes added to remove/add curves
        private void hrCbx_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem toggledcurve = hrCurve;

            if (hrCbx.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(toggledcurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(toggledcurve);
            }

            zedGraphControl1.Refresh();
        }

        private void speedCbx_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem toggledcurve = speedCurve;

            if (speedCbx.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(toggledcurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(toggledcurve);
            }

            zedGraphControl1.Refresh();
        }

        private void cadenceCbx_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem toggledcurve = cadenceCurve;

            if (cadenceCbx.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(toggledcurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(toggledcurve);
            }

            zedGraphControl1.Refresh();
        }

        private void powerCbx_CheckedChanged(object sender, EventArgs e)
        {
            ZedGraph.CurveItem toggledcurve = powerCurve;

            if (powerCbx.Checked)
            {
                zedGraphControl1.GraphPane.CurveList.Remove(toggledcurve);
            }
            else
            {
                zedGraphControl1.GraphPane.CurveList.Add(toggledcurve);
            }

            zedGraphControl1.Refresh();
        }

        //CB Pie chart created to display power balance
        private void CreatePie(ZedGraphControl zgc2)
        {
            // get a reference to the GraphPane
            myPane2 = zgc2.GraphPane;

            myPane2.CurveList.Clear();
            myPane2.Title.Text = "Power Balance";
            myPane2.Title.FontSpec.Size = 30;
            myPane2.Legend.FontSpec.Size = 25;            

            PieItem item1 = myPane2.AddPieSlice(avgLeft, Color.Red, 0, "Left");
            PieItem item2 = myPane2.AddPieSlice(avgRight, Color.Blue, 0, "Right");

            item1.LabelType = PieLabelType.Name_Percent;
            item1.LabelDetail.FontSpec.Size = 23;
            item2.LabelType = PieLabelType.Name_Percent;
            item2.LabelDetail.FontSpec.Size = 23;

            // Tell ZedGraph to refigure the
            // axes since the data have changed
            zgc2.AxisChange();
            zgc2.Invalidate();
            zgc2.Refresh();
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "HR Max")
            {
                textBox1.Text = ""; // Clears the text field
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.Text = "HR Max"; // Returns old text if you want
            }
        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            if (textBox2.Text == "HR Rest")
            {
                textBox2.Text = ""; // Clears the text field
            }
        }

        private void textBox2_Leave(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                textBox2.Text = "HR Max"; // Returns old text if you want
            }
        }

        //CB used to calculate where zone text should be displayed based on the values entered by user
        public void zoneCalc(double zoneValue)
        {            
            double sOffset = yMax - yMin; //eg 120 = 200 - 80
            double stepAmount = sOffset / 10; //eg 12 = 120 / 10
            double cfMj = 1 / stepAmount; //eg 0.08333 = 1 / 12
            double cfMn = cfMj / 10; //eg 0.008333 = 0.08333 / 10
            double aConversion = zoneValue - yMin; //eg 52 = 132 - 80
            double invResult = aConversion * cfMn; //eg 0.4333 = 52 * 0.008333
            result = 1 - invResult; //eg 0.56667 - 1 / 0.4333

        }

        private void button2_Click(object sender, EventArgs e)
        {
            axisSwitch = false;
            CreateGraph(zedGraphControl1);
            yMax = myPane.YAxis.Scale.Max;
            yMin = myPane.YAxis.Scale.Min;
            myPane.GraphObjList.Clear();
            double hrMax, hrRest, hrReserve, zone50, zone60, zone70, zone80, zone90, zone100;
            hrMax = Convert.ToDouble(textBox1.Text);
            hrRest = Convert.ToDouble(textBox2.Text);
            //CB Heart rate zones calculated
            hrReserve = hrMax - hrRest;
            zone50 = hrReserve * 0.5 + hrRest;
            zone60 = hrReserve * 0.6 + hrRest;
            zone70 = hrReserve * 0.7 + hrRest;
            zone80 = hrReserve * 0.8 + hrRest;
            zone90 = hrReserve * 0.9 + hrRest;
            zone100 = hrReserve + hrRest;
            //CB makes sure zones are displayed across the whole of the x-axis
            XDate start;
            start = dateFirst;
            start.AddYears(-1);
            XDate end;
            end = dateFirst;
            end.AddYears(1);            

            //MessageBox.Show(yMax.ToString());
            //MessageBox.Show(yMin.ToString());
            //double scale50, scale60, scale70, scale80, scale90, scale100;
            //scale50 = zone50 / 120;

            //CB creates colour boxes based on inputted values and draws them on to the graph
            BoxObj zone50Box = new BoxObj(start, zone50, end, zone50, Color.Empty, Color.Empty);
            zone50Box.Fill = new Fill(Color.Empty);
            zone50Box.ZOrder = ZOrder.E_BehindCurves;
            zone50Box.IsClippedToChartRect = true;
            zone50Box.Location.CoordinateFrame = CoordType.AxisXYScale;

            BoxObj zone60Box = new BoxObj(start, zone60, end, zone60 - zone50, Color.Empty, Color.FromArgb(100, Color.LightGray));            
            zone60Box.Fill = new Fill(Color.LightGray, Color.FromArgb(120, Color.LightGray), 45.0F);
            zone60Box.ZOrder = ZOrder.E_BehindCurves;
            zone60Box.IsClippedToChartRect = true;
            zone60Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj zone60Text = new TextObj("Very Light", 0, 0);
            zone60Text.Location.CoordinateFrame = CoordType.ChartFraction;
            zone60Text.Location.AlignH = AlignH.Left;
            zone60Text.Location.AlignV = AlignV.Top;
            zoneCalc(zone60);
            zone60Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(zone60).ToString());
            zone60Text.ZOrder = ZOrder.E_BehindCurves;
            zone60Text.FontSpec.Border.IsVisible = false; // Disable the border
            zone60Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(zone60Text);

            BoxObj zone70Box = new BoxObj(start, zone70, end, zone70 - zone60, Color.Empty, Color.FromArgb(100, Color.LightBlue));
            zone70Box.Fill = new Fill(Color.LightBlue, Color.FromArgb(120, Color.LightBlue), 45.0F);
            zone70Box.ZOrder = ZOrder.E_BehindCurves;
            zone70Box.IsClippedToChartRect = true;
            zone70Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj zone70Text = new TextObj("Light", 0, 0);
            zone70Text.Location.CoordinateFrame = CoordType.ChartFraction;
            zone70Text.Location.AlignH = AlignH.Left;
            zone70Text.Location.AlignV = AlignV.Top;
            zoneCalc(zone70);
            zone70Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(zone70).ToString());
            zone70Text.ZOrder = ZOrder.E_BehindCurves;
            zone70Text.FontSpec.Border.IsVisible = false; // Disable the border
            zone70Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(zone70Text);

            BoxObj zone80Box = new BoxObj(start, zone80, end, zone80 - zone70, Color.Empty, Color.FromArgb(100, Color.LightGreen));
            zone80Box.Fill = new Fill(Color.LightGreen, Color.FromArgb(120, Color.LightGreen), 45.0F);
            zone80Box.ZOrder = ZOrder.E_BehindCurves;
            zone80Box.IsClippedToChartRect = true;
            zone80Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj zone80Text = new TextObj("Moderate", 0, 0);
            zone80Text.Location.CoordinateFrame = CoordType.ChartFraction;
            zone80Text.Location.AlignH = AlignH.Left;
            zone80Text.Location.AlignV = AlignV.Top;
            zoneCalc(zone80);
            zone80Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(zone80).ToString());
            zone80Text.ZOrder = ZOrder.E_BehindCurves;
            zone80Text.FontSpec.Border.IsVisible = false; // Disable the border
            zone80Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(zone80Text);

            BoxObj zone90Box = new BoxObj(start, zone90, end, zone90 - zone80, Color.Empty, Color.FromArgb(100, Color.Orange));
            zone90Box.Fill = new Fill(Color.Orange, Color.FromArgb(120, Color.Orange), 45.0F);
            zone90Box.ZOrder = ZOrder.E_BehindCurves;
            zone90Box.IsClippedToChartRect = true;
            zone90Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj zone90Text = new TextObj("Hard", 0, 0);
            zone90Text.Location.CoordinateFrame = CoordType.ChartFraction;
            zone90Text.Location.AlignH = AlignH.Left;
            zone90Text.Location.AlignV = AlignV.Top;
            zoneCalc(zone90);
            zone90Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(zone90).ToString());
            zone90Text.ZOrder = ZOrder.E_BehindCurves;
            zone90Text.FontSpec.Border.IsVisible = false; // Disable the border
            zone90Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(zone90Text);

            BoxObj zone100Box = new BoxObj(start, zone100, end, zone100 - zone90, Color.Empty, Color.FromArgb(100, Color.Crimson));
            zone100Box.Fill = new Fill(Color.Crimson, Color.FromArgb(120, Color.Crimson), 45.0F);
            zone100Box.ZOrder = ZOrder.E_BehindCurves;
            zone100Box.IsClippedToChartRect = true;
            zone100Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj zone100Text = new TextObj("Maximum", 0, 0);
            zone100Text.Location.CoordinateFrame = CoordType.ChartFraction;
            zone100Text.Location.AlignH = AlignH.Left;
            zone100Text.Location.AlignV = AlignV.Top;
            zoneCalc(zone100);
            zone100Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(zone100).ToString());
            zone100Text.ZOrder = ZOrder.E_BehindCurves;
            zone100Text.FontSpec.Border.IsVisible = false; // Disable the border
            zone100Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(zone100Text);

            zedGraphControl1.GraphPane.GraphObjList.Add(zone50Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(zone60Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(zone70Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(zone80Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(zone90Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(zone100Box);

            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            zedGraphControl1.Refresh();
        }

        //CB Intensity Factor calculated
        public void intensityFactor(double np, double tp)
        {
            iFactor = np / tp;
            iFactor = Math.Round(iFactor, 2);
        }
        //CB Training Stress Score calculated
        public void tss(double np, double tp, double iFac, double sec)
        {
            trainingSS = (sec * np * iFac) / (tp * 3600) * 100;
            trainingSS = Math.Round(trainingSS, 2);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            axisSwitch = true;
            CreateGraph(zedGraphControl1);
            yMax = myPane.YAxis.Scale.Max;
            yMin = myPane.YAxis.Scale.Min;
            myPane.GraphObjList.Clear();
            double ftp, level1, level2, level3, level4, level5, level6;
            ftp = Convert.ToDouble(textBox4.Text);

            intensityFactor(nPower, ftp);
            lblFactor.Text = "Intensity Factor: " + iFactor.ToString();
            tss(nPower, ftp, iFactor, raceSeconds);
            lblTss.Text = "Training Stress Score: " + trainingSS.ToString();

            //CB power zones calculated
            level1 = ftp * 0.55;
            level2 = ftp * 0.75;
            level3 = ftp * 0.9;
            level4 = ftp * 1.05;
            level5 = ftp * 1.2;
            level6 = ftp * 1.5;

            XDate start;
            start = dateFirst;
            start.AddYears(-1);
            XDate end;
            end = dateFirst;
            end.AddYears(1);

            BoxObj level1Box = new BoxObj(start, level1, end, level1, Color.Empty, Color.FromArgb(100, Color.WhiteSmoke));
            level1Box.Fill = new Fill(Color.WhiteSmoke, Color.FromArgb(120, Color.WhiteSmoke), 45.0F);
            level1Box.ZOrder = ZOrder.E_BehindCurves;
            level1Box.IsClippedToChartRect = true;
            level1Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level1Text = new TextObj("Active Recovery", 0, 0);
            level1Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level1Text.Location.AlignH = AlignH.Left;
            level1Text.Location.AlignV = AlignV.Top;
            zoneCalc(level1);
            level1Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(level2).ToString());
            level1Text.ZOrder = ZOrder.E_BehindCurves;
            level1Text.FontSpec.Border.IsVisible = false; // Disable the border
            level1Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(level1Text);

            BoxObj level2Box = new BoxObj(start, level2, end, level2 - level1, Color.Empty, Color.FromArgb(100, Color.LightGray));
            level2Box.Fill = new Fill(Color.LightGray, Color.FromArgb(120, Color.LightGray), 45.0F);
            level2Box.ZOrder = ZOrder.E_BehindCurves;
            level2Box.IsClippedToChartRect = true;
            level2Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level2Text = new TextObj("Endurance", 0, 0);
            level2Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level2Text.Location.AlignH = AlignH.Left;
            level2Text.Location.AlignV = AlignV.Top;
            zoneCalc(level2);
            level2Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(level2).ToString());
            level2Text.ZOrder = ZOrder.E_BehindCurves;
            level2Text.FontSpec.Border.IsVisible = false; // Disable the border
            level2Text.FontSpec.Fill.IsVisible = false;
            level2Text.FontSpec.FontColor = Color.GhostWhite;
            myPane.GraphObjList.Add(level2Text);

            BoxObj level3Box = new BoxObj(start, level3, end, level3 - level2, Color.Empty, Color.FromArgb(100, Color.LightBlue));
            level3Box.Fill = new Fill(Color.LightBlue, Color.FromArgb(120, Color.LightBlue), 45.0F);
            level3Box.ZOrder = ZOrder.E_BehindCurves;
            level3Box.IsClippedToChartRect = true;
            level3Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level3Text = new TextObj("Tempo", 0, 0);
            level3Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level3Text.Location.AlignH = AlignH.Left;
            level3Text.Location.AlignV = AlignV.Top;
            zoneCalc(level3);
            level3Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(level3).ToString());
            level3Text.ZOrder = ZOrder.E_BehindCurves;
            level3Text.FontSpec.Border.IsVisible = false; // Disable the border
            level3Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(level3Text);

            BoxObj level4Box = new BoxObj(start, level4, end, level4 - level3, Color.Empty, Color.FromArgb(100, Color.LightGreen));
            level4Box.Fill = new Fill(Color.LightGreen, Color.FromArgb(120, Color.LightGreen), 45.0F);
            level4Box.ZOrder = ZOrder.E_BehindCurves;
            level4Box.IsClippedToChartRect = true;
            level4Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level4Text = new TextObj("Lactate Threshold", 0, 0);
            level4Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level4Text.Location.AlignH = AlignH.Left;
            level4Text.Location.AlignV = AlignV.Top;
            zoneCalc(level4);
            level4Text.Location.Y = result;
            //MessageBox.Show(zoneCalc(level4).ToString());
            level4Text.ZOrder = ZOrder.E_BehindCurves;
            level4Text.FontSpec.Border.IsVisible = false; // Disable the border
            level4Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(level4Text);

            BoxObj level5Box = new BoxObj(start, level5, end, level5 - level4, Color.Empty, Color.FromArgb(100, Color.Orange));
            level5Box.Fill = new Fill(Color.Orange, Color.FromArgb(120, Color.Orange), 45.0F);
            level5Box.ZOrder = ZOrder.E_BehindCurves;
            level5Box.IsClippedToChartRect = true;
            level5Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level5Text = new TextObj("VO\x2082 Max", 0, 0);
            level5Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level5Text.Location.AlignH = AlignH.Left;
            level5Text.Location.AlignV = AlignV.Top;
            zoneCalc(level5);
            level5Text.Location.Y = result;
            level5Text.ZOrder = ZOrder.E_BehindCurves;
            level5Text.FontSpec.Border.IsVisible = false; // Disable the border
            level5Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(level5Text);

            BoxObj level6Box = new BoxObj(start, level6, end, level6 - level5, Color.Empty, Color.FromArgb(100, Color.Crimson));
            level6Box.Fill = new Fill(Color.Crimson, Color.FromArgb(120, Color.Crimson), 45.0F);
            level6Box.ZOrder = ZOrder.E_BehindCurves;
            level6Box.IsClippedToChartRect = true;
            level6Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level6Text = new TextObj("Anaerobic Capacity", 0, 0);
            level6Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level6Text.Location.AlignH = AlignH.Left;
            level6Text.Location.AlignV = AlignV.Top;
            zoneCalc(level6);
            level6Text.Location.Y = result;            
            level6Text.ZOrder = ZOrder.E_BehindCurves;
            level6Text.FontSpec.Border.IsVisible = false; // Disable the border
            level6Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(level6Text);

            BoxObj level7Box = new BoxObj(start, myPane.YAxis.Scale.Max, end, myPane.YAxis.Scale.Max - level6, Color.Empty, Color.FromArgb(100, Color.Firebrick));
            level7Box.Fill = new Fill(Color.Firebrick, Color.FromArgb(120, Color.Firebrick), 45.0F);
            level7Box.ZOrder = ZOrder.E_BehindCurves;
            level7Box.IsClippedToChartRect = true;
            level7Box.Location.CoordinateFrame = CoordType.AxisXYScale;
            TextObj level7Text = new TextObj("Neuromuscular Power", 0, 0);
            level7Text.Location.CoordinateFrame = CoordType.ChartFraction;
            level7Text.Location.AlignH = AlignH.Left;
            level7Text.Location.AlignV = AlignV.Top;
            level7Text.ZOrder = ZOrder.E_BehindCurves;
            level7Text.FontSpec.Border.IsVisible = false; // Disable the border
            level7Text.FontSpec.Fill.IsVisible = false;
            myPane.GraphObjList.Add(level7Text);

            zedGraphControl1.GraphPane.GraphObjList.Add(level1Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(level2Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(level3Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(level4Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(level5Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(level6Box);
            zedGraphControl1.GraphPane.GraphObjList.Add(level7Box);
            double ftpText = Convert.ToDouble(textBox4.Text);
            if (ftpText <= 240)
            {
                level1Text.FontSpec.Size = 10;
                level2Text.FontSpec.Size = 10;
                level3Text.FontSpec.Size = 10;
                level4Text.FontSpec.Size = 10;
                level5Text.FontSpec.Size = 10;
                level6Text.FontSpec.Size = 10;
                level7Text.FontSpec.Size = 10;
            }

            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            zedGraphControl1.Refresh();
        }

        private void textBox4_Enter(object sender, EventArgs e)
        {
            if (textBox4.Text == "FTP")
            {
                textBox4.Text = ""; //CB Clears the text field
            }
        }

        private void textBox4_Leave(object sender, EventArgs e)
        {
            if (textBox4.Text == "")
            {
                textBox4.Text = "FTP"; //CB Returns old text
            }
        }

    }
}
