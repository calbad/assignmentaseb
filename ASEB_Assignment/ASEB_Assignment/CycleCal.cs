﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calendar.NET;
using System.Data.SqlServerCe;

namespace ASEB_Assignment
{
    public partial class CycleCal : Form
    {
        //CB Database connection created
        SqlCeConnection mySqlConn = new SqlCeConnection(@"Data Source=G:\Level 6\ASEB\assignmentaseb\ASEB_Assignment\ASEBDatabase.sdf");
        DateTime calDate, listDate;
        public CycleCal()
        {
            InitializeComponent();
            mySqlConn.Open(); //CB Opens DB connection
            CreateCalendar();
        }

        public void CreateCalendar()
        {
            //CB selects all dates from database and adds these to the calendar
            SqlCeCommand cmdDates = new SqlCeCommand("select date from RaceDate", mySqlConn);
            SqlCeDataReader reader = cmdDates.ExecuteReader();
            while (reader.Read())
            {
                calDate = Convert.ToDateTime(reader[0]);
                var raceEvent = new CustomEvent
                {
                    Date = calDate,
                    RecurringFrequency = RecurringFrequencies.None,
                    EventText = "Race"
                };
                calendar1.AddEvent(raceEvent);
                //CB also added to listbox so can be loaded to Form1
                listBox1.Items.Add(calDate);
            }
        }
        //CB if user double clicks on date loads corresponding data to to Form1
        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox1.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                listDate = Convert.ToDateTime(listBox1.SelectedItem);
                calendar1.CalendarDate = listDate;
                Form1 form1 = new Form1();
                SqlCeCommand cmdDates = new SqlCeCommand("select date, version, monitor, smode, startDate, startTime, length, interval, entries, lenHours from RaceDate where date = '" + listDate + "'", mySqlConn);
                SqlCeDataReader reader = cmdDates.ExecuteReader();
                while (reader.Read())
                {                    
                    form1.lblVersion.Text = reader[1].ToString();
                    form1.lblMonitor.Text = reader[2].ToString();
                    form1.lblMode.Text = reader[3].ToString();
                    form1.lblDate.Text = reader[4].ToString();
                    var dateText = reader[4].ToString().Split('='); //CB Split used to create area of data so usable data can be extracted and used later
                    form1.date = dateText[1];
                    form1.lblTime.Text = reader[5].ToString();
                    var timeText = reader[5].ToString().Split('='); //CB Split used to create area of data so usable data can be extracted and used later
                    form1.time = timeText[1];
                    form1.lblLength.Text = reader[6].ToString();
                    form1.lblInterval.Text = reader[7].ToString();
                    var intervalText = reader[7].ToString().Split('='); //CB Split used to create area of data so usable data can be extracted and used later
                    form1.interval = Convert.ToDouble(intervalText[1]);
                    form1.lenHours = Convert.ToDouble(reader[9]);
                } 
                SqlCeCommand cmdData = new SqlCeCommand("select hr, speed, cadence, altitude, power, balance from RaceData where start = '" + listDate + "'", mySqlConn);
                SqlCeDataReader readerB = cmdData.ExecuteReader();
                form1.listBox1.Items.Clear();
                string all;
                while (readerB.Read())
                {
                    if (readerB[5] == DBNull.Value)
                    {
                        all = readerB[0].ToString() + "\t" + readerB[1].ToString() + "\t" + readerB[2].ToString() + "\t" + readerB[3].ToString() + "\t" + readerB[4].ToString();
                    }
                    else
                    {
                        all = readerB[0].ToString() + "\t" + readerB[1].ToString() + "\t" + readerB[2].ToString() + "\t" + readerB[3].ToString() + "\t" + readerB[4].ToString() + "\t" + readerB[5].ToString();
                    }
                    form1.listBox1.Items.Add(all);
                    //MessageBox.Show(all);
                }                
                form1.listToGrid();                
                this.Hide();
                form1.Show();
                
            }            
        }
        //CB if user single clicks changes calendar to show the selected race
        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            int index = this.listBox1.IndexFromPoint(e.Location);
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                listDate = Convert.ToDateTime(listBox1.SelectedItem);
                calendar1.CalendarDate = listDate;
            }
        }
        //CB ensures if calendar is closed Form1 is reopened
        private void CycleCal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
        }
    }
}
