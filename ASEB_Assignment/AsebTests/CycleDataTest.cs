﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms; //added to gain access to Form elements. Also added a reference to windows.forms 
using ASEB_Assignment;
using ZedGraph;
using System.Data.SqlServerCe;

namespace AsebTests
{
    [TestClass]
    public class CycleDataTest
    {
        [TestMethod]
        public void TestMethod1()
        {
        }

        [TestMethod]
        public void kmhToMph()
        {
            // Arrange
            double kmh = 1;
            double expected = Convert.ToDouble(0.62);
            Form1 test = new Form1();           

            // Act
            test.kmhToMph(kmh);

            // Assert
            double actual = test.mphSpeed;
            Assert.AreEqual(expected, actual, 0.001, "Speed Not Converted Correctly");
        }

        [TestMethod]
        public void mphToKmh()
        {
            // Arrange
            double mph = 1;
            double expected = Convert.ToDouble(1.61);
            Form1 test = new Form1();

            // Act
            test.mphToKmh(mph);

            // Assert
            double actual = test.kmhSpeed;
            Assert.AreEqual(expected, actual, 0.001, "Speed Not Converted Correctly");
        }

        [TestMethod]
        public void speedAverage()
        {
            // Arrange
            double speedSum = 1000;
            double rows = 100;
            double expected = 10;
            Form1 test = new Form1();

            // Act
            test.averageSpeed(rows, speedSum);

            // Assert
            double actual = test.avgSpeed;
            Assert.AreEqual(expected, actual, 0.001, "Average Speed Not Calculated Correctly");
        }

        [TestMethod]
        public void zoneCalc()
        {
            // Arrange
            double zoneValue = 132;
            double expected = 0.5666;
            Graph test = new Graph();
            test.yMax = 200;
            test.yMin = 80;

            // Act
            test.zoneCalc(zoneValue);

            // Assert
            double actual = test.result;
            Assert.AreEqual(expected, actual, 0.001, "Zone Not Calculated Correctly");
        }

        [TestMethod]
        public void normalPower()
        {
            // Arrange
            double firstAvg = 100;
            double secondAvg = 200;
            double thirdAvg = 300;
            double expected = 239;
            Form1 test = new Form1();

            // Act
            test.normalisedPower(firstAvg, secondAvg, thirdAvg);

            // Assert
            double actual = test.normalP;
            Assert.AreEqual(expected, actual, 0.1, "Normalised Power Calculated Incorrectly");
        }

        [TestMethod]
        public void intFactor()
        {
            // Arrange
            double np = 210;
            double tp = 280;
            double expected = 0.75;
            Graph test = new Graph();

            // Act
            test.intensityFactor(np, tp);

            // Assert
            double actual = test.iFactor;
            Assert.AreEqual(expected, actual, 0.001, "Intensity Factor Calculated Incorrectly");
        }

        [TestMethod]
        public void trainingStress()
        {
            // Arrange
            double np = 200;
            double tp = 320;
            double iFac = 1;
            double sec = 60;
            double expected = 1.04;
            Graph test = new Graph();            

            // Act
            test.tss(np, tp, iFac, sec);

            // Assert
            double actual = test.trainingSS;
            Assert.AreEqual(expected, actual, 0.001, "Training Stress Score Calculated Incorrectly");
        }
       
    }
}
